from copy import deepcopy
from inspect import isfunction, isclass

'''
WARNING! Use this module at your own risk, it's kinda buggy :)
'''


def deep_copy(obj):
    if isclass(obj):
        return class_deep_copy(obj)

    if isfunction(obj):
        return function_deep_copy(obj)

    try:
        return deepcopy(obj)
    except TypeError:
        return obj


def class_deep_copy(cls: type, name=None):
    if not name:
        name = cls.__name__
    new_cls = deepcopy(type(name, cls.__bases__, dict(cls.__dict__)))
    for item_name in set(dir(cls)) - {'__class__', '__dict__'}:
        item = getattr(cls, item_name)
        try:
            new_item = deep_copy(item)
        except TypeError:
            new_item = item

        setattr(new_cls, item_name, new_item)
    return new_cls


def function_deep_copy(function):
    new_method = lambda *args, **kwargs: function(*args, **kwargs)
    for item_name in dir(function):
        item = getattr(function, item_name)
        if item_name.startswith('__') and item_name.endswith('__') and callable(item):
            continue
        try:
            setattr(new_method, item_name, deep_copy(item))
        except (AttributeError, ValueError):
            pass
    return new_method

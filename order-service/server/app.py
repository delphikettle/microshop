from importlib import import_module

import tornado_swirl as swirl

import_module('server.api')


class OrderServiceApplication(swirl.Application):
    def __init__(self):
        settings = {
            'serve_traceback': True
        }
        super().__init__(swirl.api_routes(), **settings)

import asyncio
import json
from concurrent.futures import ThreadPoolExecutor
from datetime import datetime
from multiprocessing import Process
from urllib.parse import urljoin

import uvloop
from aio_pika import IncomingMessage, connect_robust, Message
from aiohttp import ClientSession, ClientConnectionError

from server.config import PORT, MQ_URL


class MessagesWorker(Process):
    _queues = {}
    _connection = None
    _channel = None

    def __init__(self):
        super().__init__()
        self.loop = uvloop.new_event_loop()
        self.loop.set_default_executor(ThreadPoolExecutor())
        self.session: ClientSession = None

    def run(self):
        self.loop.create_task(self.arun())
        print(" [*] Listening for messages")
        self.loop.run_forever()

    @classmethod
    async def get_connection(cls):
        if not cls._connection:
            cls._connection = await connect_robust(MQ_URL)
        return cls._connection

    @classmethod
    async def get_channel(cls):
        if not cls._channel:
            cls._channel = await (await cls.get_connection()).channel()
        return cls._channel

    @classmethod
    async def get_queue(cls, queue):
        if queue not in cls._queues:
            channel = await cls.get_channel()
            cls._queues[queue] = await channel.declare_queue(queue)
        return cls._queues[queue]

    async def arun(self):
        self.session = ClientSession()
        self.queue = await self.get_queue('order')
        await self.queue.consume(self.on_message)

    async def on_message(self, message: IncomingMessage):
        query: dict = json.loads(message.body.decode())
        url = query['url']
        method = query.get('method', 'get').lower()
        data = query.get('data')
        try:
            async with self.session.request(
                    method,
                    urljoin(f'http://localhost:{PORT}/', url),
                    data=data
            ) as resp:
                if 'from' in query:
                    await self.send_message(query['from'] + '_answers', {
                        'query_id': query.get('query_id'),
                        'answer': await resp.json(),
                        'code': resp.status
                    })
                message.ack()
        except ClientConnectionError:
            message.reject()

    @classmethod
    async def send_message(cls, queue_name, mes_object):
        channel = await cls.get_channel()

        await channel.default_exchange.publish(
            Message(json.dumps(mes_object).encode()),
            routing_key=queue_name
        )

    @classmethod
    async def wait_for_resp(cls, query_id):
        queue = await cls.get_queue('order_answers')
        async with queue.iterator() as queue_iter:
            async for message in queue_iter:
                data = json.loads(message.body.decode())
                if data.get('query_id') == query_id and 'code' in data:
                    message.ack()
                    return data

    @classmethod
    async def request_via_mes(cls, service, url, method='get', data=None, wait_for_resp=False):
        mes = {
            'from': 'order',
            'url': url,
            'method': method
        }
        resp_task = None
        if wait_for_resp:
            mes['query_id'] = datetime.now().microsecond
            mes['from'] = 'order'
            resp_task = asyncio.get_event_loop().create_task(cls.wait_for_resp(mes['query_id']))

        if data is not None:
            mes['data'] = data

        await cls.send_message(service, mes)
        if wait_for_resp:
            return await resp_task
